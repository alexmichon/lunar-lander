/*
 name:               Alex Michon
 ONE Card number:    1392360
 Unix id:            amichon
 lecture section:    A1
 lab section:        D01
 TA's name:          Chenyang Huang 

 Used http://alienryderflex.com/intersect/ for line intersection
 Used ball.c for ncurses and handler
 Used Lab3 and lecture slide 47 for popen sketchpad
*/

#define _XOPEN_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>
#include <ncurses.h>
#include <signal.h>
#include <sys/time.h>
#include "lander.h"

/*######################################################################
 MAIN
 ######################################################################*/

int main(int argc, char *argv[])
{

	char exec_name[] = "java -jar Sketchpad.jar";

	FILE *landscape_image;

	int i;
	int rval;
	int character;
	int running=1;
	int point_count=0;

	double ship_init_x=ship_start_x;
	double ship_init_y=ship_start_y;

	struct sigaction handler;
	struct itimerval timer;
    
    init_ncurses();

/*---------------------------------------------------------------------------*/

	if(argc<5)
	{
		printf("need flags -g gravity, -t thrust, -f landscape.txt (optional)\n");
		exit(EXIT_FAILURE);
	}

/*---------------------------------------------------------------------------*/

	for(i=0;i<argc;i++)
	{
	
		/* Gavity */

		if(strncmp(argv[i],"-g",2)==0)
		{
			if(argv[i+1]==NULL)
			{
				printf("bad '-g', or good option but no value.\n");
				exit(EXIT_FAILURE);
			}

			else
			{
				rval=sscanf(argv[i+1],"%lf",&gravity);

				if(rval != 1)
				{
					printf("Missing flag -g gravity\n");
					exit(EXIT_FAILURE);
				}

				if(gravity<0 || gravity>20)
				{
					printf("gravity < 0, > 20 not allowed\n");
					exit(EXIT_FAILURE);
				}
			}
		}

	/*---------------------------------------------------------------------------*/

		/* Thrust */

		else if(strncmp(argv[i],"-t",2)==0)
		{
			if(argv[i+1]==NULL)
			{
				printf("bad '-t', or good option but no value.\n");
				exit(EXIT_FAILURE);
			}

			else
			{
				rval=sscanf(argv[i+1],"%lf",&thrust);

				if(rval != 1)
				{
					printf("Missing flag -t thrust\n");
					exit(EXIT_SUCCESS);
				}

				if(thrust>0 || thrust<(-20))
				{
					printf("thrust > 0, < -20 not allowed\n");
					exit(EXIT_FAILURE);
				}

				time2=-thrust/100;
			}
		}

	/*---------------------------------------------------------------------------*/

		/* Landscape */

		else if(strncmp(argv[i],"-f",2)==0)
		{
			if(argv[i+1]==NULL)
			{
				printf("bad '-f', or good option but no value.\n");
				exit(EXIT_FAILURE);
			}

			else
			{
				if(strncmp(argv[i+1],"custom",6)==0)
				{
					no_landscape=0;
				}

				else
				{
					landscape_image = fopen( argv[i+1], "r" );

					no_landscape=1;

					if(landscape_image==NULL)
				    	{
						printf("could not open %s\n",argv[i+1]);
						exit(EXIT_FAILURE);
				    	}
				}
			}
		}

	/*---------------------------------------------------------------------------*/

		// additional option (fuel)
		else if(strncmp(argv[i],"-i",2)==0)
		{
			rval=sscanf(argv[i+1],"%d",&amount_of_fuel);

			if(rval != 1)
			{
				exit(EXIT_SUCCESS);
			}

			if(amount_of_fuel<0 || amount_of_fuel>100)
			{
				printf("Fuel < 0, > 100 not allowed\n");
				exit(EXIT_FAILURE);
			}

			amount_of_fuel=(amount_of_fuel+1)*2;

			use_addition=1;

			lower_f=upper_f+(amount_of_fuel);
			lower=lower_f+2;
		}

	/*---------------------------------------------------------------------------*/

		// additional option (choose ship)
		else if(strncmp(argv[i],"-c",2)==0)
		{
			chooses=1;
		}

	} //CLOSE: FOR

/*---------------------------------------------------------------------------*/

	/* Open Sketchpad */

	fppo=popen(exec_name,"w");

    	if(fppo==NULL)
    	{
        	printf("Unable to pipe to sketchpad\n");
        	exit(EXIT_FAILURE);
    	}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

	if(chooses==1)
	{    

	    make_ship1(ship_start_x_1,ship_start_y,largest,smallest);
	    make_ship2(ship_start_x_2,ship_start_y,largest,smallest);
	    make_ship3(ship_start_x_3,ship_start_y,largest,smallest);
	    make_arrow(ship_start_x_1,arrow_y);
	    draw_ships(ship1);
	    draw_ships(ship2);
	    draw_ships(ship3);
        
	    mvprintw(5,10,"Choose your ship");
	    mvprintw(6,10,"(Using arrow keys)");
	    mvprintw(7,10,"Press 'c' to continue");
	    refresh();
	    
	    nodelay( stdscr, TRUE );
	    
	    handler.sa_handler = handle_arrow;
	    
	    sigemptyset( &handler.sa_mask );
	    handler.sa_flags = 0;
	    
	    if( sigaction( SIGALRM, &handler, NULL ) < 0 )
	    {
		exit( EXIT_FAILURE );
	    }
	    
	    timer.it_value.tv_sec = 0;
	    timer.it_value.tv_usec = 50000;
	    timer.it_interval.tv_sec = 0;
	    timer.it_interval.tv_usec = 50000;
	    
	    if( setitimer( ITIMER_REAL, &timer, NULL ) < 0 )
	    {
		exit( EXIT_FAILURE );
	    }

	    while(running)
	    {
		character=getch();
		
		switch( character )
		{
		        
		    case KEY_LEFT:
		        if(arrow.init_x>ship_start_x_1)
		        {
		            arrow.init_x=arrow.init_x-100;
		        }
		        break;
		        
		    case KEY_RIGHT:
		        if(arrow.init_x<ship_start_x_3)
		        {
		            arrow.init_x=arrow.init_x+100;
		        }
		        break;
		    case 'c':
		        running=0;
		        refresh();
		        break;
		}

	    }
        
	    erase_ships(ship1);
	    erase_ships(ship2);
	    erase_ships(ship3);
	    
	    if(arrow.init_x==ship_start_x_1)
	    {
		this_ship=ship1;
	    }
	    
	    else if(arrow.init_x==ship_start_x_2)
	    {
		this_ship=ship2;
	    }
	    
	    else if(arrow.init_x==ship_start_x_3)
	    {
		this_ship=ship3;
	    }

	} // END IF

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

	/* initialize landscape */
	
	// if there is a given landscape
	if(no_landscape==1)
	{
		make_landscape(landscape_image);
	}

/*---------------------------------------------------------------------------*/

	// if there is no givin landscape
	else
	{
		cursor.x=cursor_start_x;
		cursor.y=cursor_start_y;

		mvprintw( 5, 10, "No Valid Landscape Given" );
		mvprintw( 6, 10, "Use keys to move cursor" );
		mvprintw( 7, 10, "Use space bar to save a point" );
		mvprintw( 8, 10, "(Up to 20 points)" );
		mvprintw( 9, 10, "Press 'c' To Continue" );
		refresh();

		this_landscape.points=0;

		running=1;

		nodelay( stdscr, TRUE );

	/*---------------------------------------------------------------------------*/

		handler.sa_handler = handle_cursor;

		sigemptyset( &handler.sa_mask );
		handler.sa_flags = 0;

		if( sigaction( SIGALRM, &handler, NULL ) < 0 )
		{
			exit( EXIT_FAILURE );
		}

		timer.it_value.tv_sec = 0;
		timer.it_value.tv_usec = 50000;
		timer.it_interval.tv_sec = 0;
		timer.it_interval.tv_usec = 50000;

		if( setitimer( ITIMER_REAL, &timer, NULL ) < 0 )
		{
			exit( EXIT_FAILURE );
		}


		while( running )
		{
			character=getch();

			if( character == ERR )
			{
				continue;
			}

			if(point_count>20)
			{
				running=0;
			}

			switch( character )
			{
				case KEY_UP:
					if(cursor.y>sketch_top)
					{
						cursor.y=cursor.y-cursor_speed;
					}
					break;

				case KEY_DOWN:
					if(cursor.y<sketch_bottom)
					{
						cursor.y=cursor.y+cursor_speed;
					}
					break;

				case KEY_LEFT:
					if(cursor.x>sketch_left)
					{
						cursor.x=cursor.x-cursor_speed;
					}
					break;

				case KEY_RIGHT:
					if(cursor.x<sketch_right)
					{
						cursor.x=cursor.x+cursor_speed;
					}
					break;

				case ' ':
					this_landscape.x_coord[this_landscape.points]=cursor.x;
					this_landscape.y_coord[this_landscape.points]=cursor.y;
                    printf("points %d",this_landscape.points);
					this_landscape.points++;

					point_count++;
		  			break;

				case 'c':
					running=0;
					refresh();
					break;

			} // CLOSE SWITCH

		} // CLOSE WHILE
        
	} // CLOSE ELSE

/*---------------------------------------------------------------------------*/

	// create landscape
	draw_landscape();
	max_min_x();
    
    // print walls
    fprintf(fppo,"drawSegment %d %d %d %d\n",
            largest,sketch_very_top,
            largest,sketch_very_bottom);
    
    fprintf(fppo,"drawSegment %d %d %d %d\n",
            smallest,sketch_very_top,
            smallest,sketch_very_bottom);

	if(chooses==0)
	{
		/* initialize ship points */
		make_ship(ship_init_x,ship_init_y,largest,smallest);
	}

/*---------------------------------------------------------------------------*/

	/* initialize fuel */
	if(use_addition==1)
	{
		left_f= largest-48;
		right_f= largest-42;
		make_fuel();
		use_fuel();
	}

/*---------------------------------------------------------------------------*/

	running=1;

	mvprintw( 5, 10, "Press any key to start." );
	refresh();
	character = getch();

	nodelay( stdscr, TRUE );

	erase();
	mvprintw( 5, 10, "Press arrow keys to rotate, spacebar for thrust and 'q' to quit." );
	refresh();

/*---------------------------------------------------------------------------*/

        handler.sa_handler = handle_signal;

	sigemptyset( &handler.sa_mask );
	handler.sa_flags = 0;

	if( sigaction( SIGALRM, &handler, NULL ) < 0 )
	{
		exit( EXIT_FAILURE );
	}

	timer.it_value.tv_sec = 0;
	timer.it_value.tv_usec = 50000;
	timer.it_interval.tv_sec = 0;
	timer.it_interval.tv_usec = 50000;

	if( setitimer( ITIMER_REAL, &timer, NULL ) < 0 )
	{
		exit( EXIT_FAILURE );
	}

/*---------------------------------------------------------------------------*/

	while( running )
	{
		character=getch();

		if( character == ERR )
		{
			continue;
		}

/*---------------------------------------------------------------------------*/

		switch( character )
		{

			case KEY_LEFT:
				if(crashed==0 && landed==0)
				{
					rotate_ship(-degrees);
				}
		  		break;

			case KEY_RIGHT:
				if(crashed==0 && landed==0)
				{
					rotate_ship(degrees);
				}
		  		break;

			case ' ':
				if(crashed==0 && landed==0)
				{
					if(use_addition==1)
					{
						if(upper_f<lower_f)
						{
							ship_thrust();
						}
					}
		
					else
					{
						ship_thrust();
					}
				}
				break;

			case 'q':
				running = 0;
		  		break;

			case 'e':
				running = 0;
				exploring = 1;
				make_human();
				draw_ship();
				break;

			default:
				break;
		}
	}

/*---------------------------------------------------------------------------*/

	running=1;

	if(exploring==1)
	{
	    erase();
	    mvprintw(5,10,"Move person and place flag");
	    mvprintw(6,10,"(Use arrow keys to move and 'f' for flag)");
	    mvprintw(7,10,"Press 'q' to quit");
	    refresh();

	    nodelay( stdscr, TRUE );
	    
	    handler.sa_handler = handle_explore;
	    
	    sigemptyset( &handler.sa_mask );
	    handler.sa_flags = 0;
	    
	    if( sigaction( SIGALRM, &handler, NULL ) < 0 )
	    {
		exit( EXIT_FAILURE );
	    }
	    
	    timer.it_value.tv_sec = 0;
	    timer.it_value.tv_usec = 50000;
	    timer.it_interval.tv_sec = 0;
	    timer.it_interval.tv_usec = 50000;
	    
	    if( setitimer( ITIMER_REAL, &timer, NULL ) < 0 )
	    {
		exit( EXIT_FAILURE );
	    }


		while( running )
		{
			character=getch();

			if( character == ERR )
			{
				continue;
			}

			if(point_count>20)
			{
				running=0;
			}

			switch( character )
			{
				case KEY_LEFT:
					if(human.x_coord>landing_line_1_x+1)
					{
						human.x_coord=human.x_coord-human_speed;
					}
					break;

				case KEY_RIGHT:
					if(human.x_coord<landing_line_2_x-1)
					{
						human.x_coord=human.x_coord+human_speed;
					}
					break;

				case 'f':
					place_flag();
					mvprintw(9,10,"PLANET CONQURED!!!");
					break;

				case 'q':
					running=0;
					break;

			} // CLOSE SWITCH

		} // CLOSE WHILE
	}

/*---------------------------------------------------------------------------*/

    shutdown_ncurses();
    
	fprintf(fppo,"end");

	fclose(fppo);

	exit(EXIT_SUCCESS);

} /* END MAIN */








/* ######################################################################
 MAKE_LANDSCAPE
 ###################################################################### */

void make_landscape(FILE *landscape_image)
{
	char landscape_coord[line_len];

	int rval;
    char *reval;
	int point_number=0;
    
	long x;
	long y;

/*---------------------------------------------------------------------------*/

	/* gets all points in file */

    while(! feof (landscape_image))
    {
        reval=fgets(landscape_coord,line_len,landscape_image);
        


        
        rval=sscanf(landscape_coord," %ld %ld",&x,&y);
        
        if(rval != landscape_rval)
        {
            printf("Unable to read lanscape");
        }
        
        this_landscape.x_coord[point_number]=x;
        this_landscape.y_coord[point_number]=y;
        
        point_number++;
    }
    
    this_landscape.points=point_number;


} /* END MAKE_LANDSCAPE */








/* ######################################################################
 DRAW_LANDSCAPE
 ###################################################################### */

void draw_landscape()
{
	int i;

	long x1;
	long x2;
	long y1;
	long y2;

/*---------------------------------------------------------------------------*/

	/* prints all points in file */

	for(i=0;i<this_landscape.points-1;i++)
	{
		x1=this_landscape.x_coord[i];
		y1=this_landscape.y_coord[i];
		x2=this_landscape.x_coord[i+1];
		y2=this_landscape.y_coord[i+1];

		fprintf(fppo,"drawSegment %ld %ld %ld %ld\n",x1,y1,x2,y2);
	}

} /* END DRAW_LANDSCAPE */








/* ######################################################################
 MAKE_SHIP
 ###################################################################### */

void make_ship(double init_x,double init_y,int largest,int smallest)
{
	this_ship.init_x=init_x;
	this_ship.init_y=init_y;

	/* bottom left point */
	this_ship.x_coord[0]= init_x-wall_len;
	this_ship.y_coord[0]= init_y+wall_len;

	/* bottom right point */
	this_ship.x_coord[1]= init_x+wall_len;
	this_ship.y_coord[1]= init_y+wall_len;

	/* top right point */
	this_ship.x_coord[2]= init_x+(wall_len/2);
	this_ship.y_coord[2]= init_y-wall_len;

	/* top left point */
	this_ship.x_coord[3]= init_x-(wall_len/2);
	this_ship.y_coord[3]= init_y-wall_len;

	/* fire point */
	this_ship.fire_point_x=init_x;
	this_ship.fire_point_y=init_y+fire_len;

	/* initialize other variables */
	this_ship.vertical_velocity=0;
	this_ship.horizontal_velocity=0;

	this_ship.angle=90;

	this_ship.flame=0;

	this_ship.points=ship_coords1;

} /* END MAKE_SHIP */








/* ######################################################################
 ROTATE_SHIP
 ###################################################################### */

void rotate_ship(int deg)
{
	int i;
	double x;
	double y;
	double rad;
	double rot_x;
	double rot_y;

        rad = deg * M_PI / oneeighty;

/*---------------------------------------------------------------------------*/
	
	/* rotate points */

	for(i = 0; i < this_ship.points; i++)
        {               
        	x=this_ship.x_coord[i]-this_ship.init_x;
        	y=this_ship.y_coord[i]-this_ship.init_y;

        	rot_x = x * cos(rad) - y * sin(rad);
        	rot_y = x * sin(rad) + y * cos(rad);

        	this_ship.x_coord[i]=rot_x+this_ship.init_x;
        	this_ship.y_coord[i]=rot_y+this_ship.init_y;
	}

/*---------------------------------------------------------------------------*/

	/* rotate fire point */

	x=this_ship.fire_point_x-this_ship.init_x;
        y=this_ship.fire_point_y-this_ship.init_y;

	rot_x = x * cos(rad) - y * sin(rad);
	rot_y = x * sin(rad) + y * cos(rad);

	this_ship.fire_point_x=rot_x+this_ship.init_x;
	this_ship.fire_point_y=rot_y+this_ship.init_y;

/*---------------------------------------------------------------------------*/

	/* save current angle */

	this_ship.angle=this_ship.angle+deg;

	if(this_ship.angle>360)
	{
		this_ship.angle=this_ship.angle-360;
	}

	else if(this_ship.angle<0)
	{
		this_ship.angle=this_ship.angle+360;
	}

} /* END ROTATE_SHIP */








/* ######################################################################
 DRAW_SHIP
 ###################################################################### */

void draw_ship()
{
	int i;

	if (this_ship.init_x<smallest)
	{
		loop_right(this_ship);
	}

	else if(this_ship.init_x>largest)
	{
		loop_left(this_ship);
	}

	if(this_ship.init_y-wall_len<sketch_very_top)
	{
		for(i=0;i<this_ship.points;i++)
		{
			this_ship.y_coord[i]=this_ship.y_coord[i]+fix_roof;
		}

		this_ship.init_y=this_ship.init_y+fix_roof;

		this_ship.fire_point_y=this_ship.fire_point_y+fix_roof;

		this_ship.vertical_velocity=0;
		
		if(this_ship.init_y<sketch_very_top)
		{
			for(i=0;i<this_ship.points;i++)
			{
				this_ship.y_coord[i]=this_ship.y_coord[i]+fix_roof*2;
			}

			this_ship.init_y=this_ship.init_y+fix_roof*2;

			this_ship.fire_point_y=this_ship.fire_point_y+fix_roof*2;
		}
	}

/*---------------------------------------------------------------------------*/

	for(i=0;i<this_ship.points;i++)
	{ 
		if(i<this_ship.points-1)
		{
			fprintf(fppo,"drawSegment %.0lf %.0lf %.0lf %.0lf\n",
				this_ship.x_coord[i],this_ship.y_coord[i],
				this_ship.x_coord[i+1],this_ship.y_coord[i+1]);
		}
		
		else
		{
			fprintf(fppo,"drawSegment %.0lf %.0lf %.0lf %.0lf\n",
				this_ship.x_coord[i],this_ship.y_coord[i],
				this_ship.x_coord[0],this_ship.y_coord[0]);
		}
	}

/*---------------------------------------------------------------------------*/

	if(this_ship.flame==1)
	{
		fprintf(fppo,"drawSegment %.0lf %.0lf %.0lf %.0lf\n",
			this_ship.x_coord[0],this_ship.y_coord[0],
			this_ship.fire_point_x,this_ship.fire_point_y);

		fprintf(fppo,"drawSegment %.0lf %.0lf %.0lf %.0lf\n",
			this_ship.fire_point_x,this_ship.fire_point_y,
			this_ship.x_coord[1],this_ship.y_coord[1]);
	}

} /* END DRAW_SHIP */








/* ######################################################################
 ERASE_SHIP
 ###################################################################### */

void erase_ship()
{
	int i;

	static int erased=0;

/*---------------------------------------------------------------------------*/

	/* erase ship points */

	for(i=0;i<this_ship.points;i++)
	{ 
		if(i<this_ship.points-1)
		{
			fprintf(fppo,"eraseSegment %.0lf %.0lf %.0lf %.0lf\n",
				this_ship.x_coord[i],this_ship.y_coord[i],
				this_ship.x_coord[i+1],this_ship.y_coord[i+1]);
		}
		
		else
		{
			fprintf(fppo,"eraseSegment %.0lf %.0lf %.0lf %.0lf\n",
				this_ship.x_coord[i],this_ship.y_coord[i],
				this_ship.x_coord[0],this_ship.y_coord[0]);
		}
	}

/*---------------------------------------------------------------------------*/

	/* erase fire_point */

	if(this_ship.flame==1)
	{
		fprintf(fppo,"eraseSegment %.0lf %.0lf %.0lf %.0lf\n",
			this_ship.x_coord[0],this_ship.y_coord[0],
			this_ship.fire_point_x,this_ship.fire_point_y);

		fprintf(fppo,"eraseSegment %.0lf %.0lf %.0lf %.0lf\n",
			this_ship.fire_point_x,this_ship.fire_point_y,
			this_ship.x_coord[1],this_ship.y_coord[1]);
	}

	if(erased==3)
	{
		this_ship.flame=0;
		erased=0;
	}

	else
	{
		erased++;
	}

} /* END ERASE_SHIP */








/* ######################################################################
 SHIP_VELOCITY
 ###################################################################### */

void ship_velocity()
{
	int i;

/*---------------------------------------------------------------------------*/

	/* move ship points */

	for(i=0;i<this_ship.points;i++)
	{
		this_ship.y_coord[i]=
			this_ship.y_coord[i] + this_ship.vertical_velocity*time1 +
			1/2*gravity*time1*time1;

		this_ship.x_coord[i]=
			this_ship.x_coord[i] + this_ship.horizontal_velocity*time2;
	}

/*---------------------------------------------------------------------------*/

	/* move center point */

	this_ship.init_y=
		this_ship.init_y + this_ship.vertical_velocity*time1 +
		1/2*gravity*time1*time1;

	this_ship.init_x=
		this_ship.init_x + this_ship.horizontal_velocity*time2;

/*---------------------------------------------------------------------------*/

	/* move fire point */

	this_ship.fire_point_y=
		this_ship.fire_point_y + this_ship.vertical_velocity*time1 +
		1/2*gravity*time1*time1;

	this_ship.fire_point_x=
		this_ship.fire_point_x + this_ship.horizontal_velocity*time2;

/*---------------------------------------------------------------------------*/

	if(gravity!=0)
	{
		this_ship.vertical_velocity=this_ship.vertical_velocity + gravity*time1;
		time1=time1+time_change;
	}

	else
	{
		this_ship.vertical_velocity=this_ship.vertical_velocity + gravity*time1;
		time1=-thrust/200;
	}

} /* END SHIP_VELOCITY */









/* ######################################################################
 SHIP_THRUST
 ###################################################################### */

void ship_thrust()
{
	int i;
	double rad;

	rad = this_ship.angle * M_PI / oneeighty;
    
        xA =thrust * cos(rad);
        
        yA =gravity + thrust * sin(rad);


/*---------------------------------------------------------------------------*/

	/* move ship points */

	for(i=0;i<this_ship.points;i++)
	{
		this_ship.x_coord[i] = this_ship.x_coord[i]+xA/soft;

		this_ship.y_coord[i] = this_ship.y_coord[i]+yA/soft;
	}

/*---------------------------------------------------------------------------*/

	/* move fire point */

	this_ship.fire_point_x = this_ship.fire_point_x+xA/soft;

	this_ship.fire_point_y = this_ship.fire_point_y+yA/soft;

/*---------------------------------------------------------------------------*/

	/* move center point */

	this_ship.init_x=this_ship.init_x+xA/soft;

	this_ship.init_y=this_ship.init_y+yA/soft;

/*---------------------------------------------------------------------------*/

	this_ship.horizontal_velocity=this_ship.horizontal_velocity + xA*time2;

	this_ship.vertical_velocity=this_ship.vertical_velocity + yA*time1;

	if(gravity!=0)
	{
		time1=time1-(time_change*sin(rad));
	}


/*---------------------------------------------------------------------------*/

	this_ship.flame=1;

	if(use_addition==1)
	{
		erase_fuel();
		use_fuel();
	}

} /* END SHIP_THRUST */








/* ######################################################################
 LOOP_RIGHT
 ###################################################################### */
void loop_right()
{
	int i;

/*---------------------------------------------------------------------------*/

	/* loop ship points */

	for(i=0;i<this_ship.points;i++)
	{
		this_ship.x_coord[i] = 
			this_ship.x_coord[i]+difference;
	}

/*---------------------------------------------------------------------------*/

	/* loop fire point */

	this_ship.fire_point_x = 
			this_ship.fire_point_x+difference;

/*---------------------------------------------------------------------------*/

	/* loop center point */

	this_ship.init_x = 
			this_ship.init_x+difference;

} /* END LOOP_RIGHT */








/* ######################################################################
 LOOP_LEFT
 ###################################################################### */
void loop_left()
{
	int i;

/*---------------------------------------------------------------------------*/

	/* loop ship points */

	for(i=0;i<this_ship.points;i++)
	{
		this_ship.x_coord[i] = 
			this_ship.x_coord[i]-difference;
	}

/*---------------------------------------------------------------------------*/

	/* loop fire point */

	this_ship.fire_point_x = 
			this_ship.fire_point_x-difference;

/*---------------------------------------------------------------------------*/

	/* loop center point */

	this_ship.init_x = 
			this_ship.init_x-difference;

} /* END LOOP_LEFT */








/* ######################################################################
 MAX_MIN_X
 ###################################################################### */
void max_min_x()
{
	int i;

	for(i=0;i<this_landscape.points;i++)
	{
		if(i==0)
		{
			largest=this_landscape.x_coord[i];
			smallest=this_landscape.y_coord[i];
		}

/*---------------------------------------------------------------------------*/

		/* largest */
		if(this_landscape.x_coord[i]>largest)
		{
			largest=this_landscape.x_coord[i];
		}

/*---------------------------------------------------------------------------*/

		/* smallest */
		else if(this_landscape.x_coord[i]<smallest)
		{
			smallest=this_landscape.x_coord[i];
		}
	}

	difference=largest-smallest;

} /* END MAX */








/* ######################################################################
 CRASH_CHECK
 ###################################################################### */
int crash_check()
{
	int i;
	int c;

	double Ax;
	double Ay;
	double Bx;
	double By;
	double Cx;
	double Cy;
	double Dx;
	double Dy;

	double distAB; 
	double theCos;
	double theSin;
	double newX;
	double ABpos;

/*---------------------------------------------------------------------------*/

	for(i=0;i<this_landscape.points-1;i++)
	{

		for(c=0;c<this_ship.points;c++)
		{

			/* check every line in lanscape */
			Ax=this_landscape.x_coord[i];
			Ay=this_landscape.y_coord[i];

			Bx=this_landscape.x_coord[i+1];
			By=this_landscape.y_coord[i+1];

/*---------------------------------------------------------------------------*/

			/* check every line in ship */
			if(c<this_ship.points-1)
			{
				Cx=this_ship.x_coord[c];
				Cy=this_ship.y_coord[c];

				Dx=this_ship.x_coord[c+1];
				Dy=this_ship.y_coord[c+1];
			}

			else
			{
				Cx=this_ship.x_coord[c];
				Cy=this_ship.y_coord[c];

				Dx=this_ship.x_coord[0];
				Dy=this_ship.y_coord[0];
			}

/*---------------------------------------------------------------------------*/

			//  (1) Translate the system so that point A is on the origin.
			Bx-=Ax;
			By-=Ay;
			Cx-=Ax;
			Cy-=Ay;
			Dx-=Ax;
			Dy-=Ay;

			//  Discover the length of segment A-B.
			distAB=sqrt(Bx*Bx+By*By);

			//  (2) Rotate the system so that point B is on the positive X axis.
			theCos=Bx/distAB;
			theSin=By/distAB;

			newX=Cx*theCos+Cy*theSin;

			Cy=Cy*theCos-Cx*theSin;
			Cx=newX;

			newX=Dx*theCos+Dy*theSin;

			Dy=Dy*theCos-Dx*theSin;
			Dx=newX;

/*---------------------------------------------------------------------------*/

			//  Fail if segment C-D doesn't cross line A-B.
			if ((Cy<0 && Dy<0) || (Cy>=0 && Dy>=0))
			{
				continue;
			}

			//  (3) Discover the position of the intersection point along line A-B.
			ABpos=Dx+(Cx-Dx)*Dy/(Dy-Cy);

			//  Fail if segment C-D crosses line A-B outside of segment A-B.
			if (ABpos<0 || ABpos>distAB)
			{
				continue;
			}

			//  Fail if the lines are parallel.
			if (Cy==Dy)
			{
				continue;
			}

			//  Success.
			landing_line_1_y=this_landscape.y_coord[i];
			landing_line_2_y=this_landscape.y_coord[i+1];

			landing_line_1_x=this_landscape.x_coord[i];
			landing_line_2_x=this_landscape.x_coord[i+1];

			return 1;

/*---------------------------------------------------------------------------*/

		}
	}

	return 0;

} /* END CRASH_CHECK */








/* ######################################################################
 INIT_NCURSES
 ###################################################################### */

void init_ncurses()
{
  int r;

  // start up the ncurses environment
  initscr(); // nothing to check, initscr exits on error

  // don't wait for enter for keyboard input
  r = cbreak(); assert( r != ERR );

  // don't echo keypresses to screen
  r = noecho(); assert( r != ERR );

  r = nonl(); assert( r != ERR );

  // turn cursor off
  r = curs_set( 0 ); assert( r != ERR );

  // don't worry about cursor position
  r = leaveok( stdscr, TRUE ); assert( r != ERR );

  // slower handling of ^C but saner output
  r = intrflush( stdscr, FALSE ); assert( r != ERR );

  // translate movement espace codes into single keys
  r = keypad( stdscr, TRUE ); assert( r != ERR );

} /* END INIT_NCURSES */








/* ######################################################################
 SHUTDOWN_NCURSES
 ###################################################################### */

void shutdown_ncurses()
{
  endwin();
    
} // END SHUTDOWN_NCURSES








/* ######################################################################
 HANDLE_SIGNAL
 ###################################################################### */

void handle_signal(int signal)
{
	if( signal != SIGALRM )
	{
		return;
	}

	if(crashed==0 && landed==0)
	{
		ship_velocity();
		draw_ship();
		fflush(fppo);
		erase_ship();
        
		// check for line intersection
		if(crash_check()==1)
		{
			//	suffiecietly slow		     upright		 flat surface
			if((this_ship.vertical_velocity<landing_speed) && (this_ship.angle==90)  && (is_flat()==1))
			{
				mvprintw(6,10,"LANDED!!!");
				refresh();

				landed=1;

				this_ship.vertical_velocity=0;
				this_ship.horizontal_velocity=0;
				gravity=0;
			}

			else
			{
				mvprintw( 6, 10,"CRASHED!!!");
				refresh();

				crashed=1;
			}
		}
	}

	if(landed==1)
	{
		mvprintw(8,10,"Press 'e' to explore");
	}

} /* END HANDLE_SIGNAL */








/* ######################################################################
 HANDLE_CURSOR
 ###################################################################### */

void handle_cursor(int signal)
{
	if( signal != SIGALRM )
	{
		return;
	}

	draw_cursor();
	draw_landscape();
	fflush(fppo);
	erase_cursor();

}// END HANDLE_CURSOR








/* ######################################################################
 DRAW_CURSOR
 ###################################################################### */

void draw_cursor()
{
	fprintf(fppo,"drawSegment %.0lf %.0lf %.0lf %.0lf\n",
		cursor.x,cursor.y-cursor_len,cursor.x,cursor.y+cursor_len);
	fprintf(fppo,"drawSegment %.0lf %.0lf %.0lf %.0lf\n",
		cursor.x-cursor_len,cursor.y,cursor.x+cursor_len,cursor.y);

} // END DRAW_CURSOR








/* ######################################################################
 ERASE_CURSOR
 ###################################################################### */

void erase_cursor()
{
	fprintf(fppo,"eraseSegment %.0lf %.0lf %.0lf %.0lf\n",
		cursor.x,cursor.y-cursor_len,cursor.x,cursor.y+cursor_len);
	fprintf(fppo,"eraseSegment %.0lf %.0lf %.0lf %.0lf\n",
		cursor.x-cursor_len,cursor.y,cursor.x+cursor_len,cursor.y);

} // END ERASE_CURSOR









/* ######################################################################
 MAKE_FUEL
 ###################################################################### */
void make_fuel()
{
	int upper= 20;
	int left= largest-50;
	int right= largest-40;

	fprintf(fppo,"drawSegment %d %d %d %d\n",left,upper,right,upper);
	fprintf(fppo,"drawSegment %d %d %d %d\n",right,upper,right,lower);
	fprintf(fppo,"drawSegment %d %d %d %d\n",right,lower,left,lower);
	fprintf(fppo,"drawSegment %d %d %d %d\n",left,lower,left,upper);

} /* END MAKE_FUEL */







/* ######################################################################
 USE_FUEL
 ###################################################################### */
void use_fuel()
{
	upper_f=upper_f+2;

	fprintf(fppo,"drawSegment %d %d %d %d\n",left_f,upper_f,right_f,upper_f);
	fprintf(fppo,"drawSegment %d %d %d %d\n",right_f,upper_f,right_f,lower_f);
	fprintf(fppo,"drawSegment %d %d %d %d\n",right_f,lower_f,left_f,lower_f);
	fprintf(fppo,"drawSegment %d %d %d %d\n",left_f,lower_f,left_f,upper_f);

} /* END USE_FUEL */







/* ######################################################################
 ERASE_FUEL
 ###################################################################### */
void erase_fuel()
{
	fprintf(fppo,"eraseSegment %d %d %d %d\n",left_f,upper_f,right_f,upper_f);
	fprintf(fppo,"eraseSegment %d %d %d %d\n",right_f,upper_f,right_f,lower_f);
	fprintf(fppo,"eraseSegment %d %d %d %d\n",right_f,lower_f,left_f,lower_f);
	fprintf(fppo,"eraseSegment %d %d %d %d\n",left_f,lower_f,left_f,upper_f);

} /* END ERASE_FUEL */








/* ######################################################################
 IS_FLAT
 ###################################################################### */

int is_flat()
{
	if(landing_line_1_y==landing_line_2_y)
	{
		return 1;
	}

	else
	{
		return 0;
	}

}// END IS_FLAT








/* ######################################################################
 HANDLE_EXPLORE
 ###################################################################### */

void handle_explore(int signal)
{
	if( signal != SIGALRM )
	{
		return;
	}

	draw_human();
	fflush(fppo);
	erase_human();

}// END HANDLE_EXPLORE








/* ######################################################################
 DRAW_HUMAN
 ###################################################################### */

void draw_human()
{
	fprintf(fppo,"drawSegment %.0lf %.0lf %.0lf %.0lf\n",
		human.x_coord,human.y_coord,
		human.x_coord,human.y_coord+wall_len);

} // END DRAW_HUMAN








/* ######################################################################
 ERASE_HUMAN
 ###################################################################### */

void erase_human()
{
	fprintf(fppo,"eraseSegment %.0lf %.0lf %.0lf %.0lf\n",
		human.x_coord,human.y_coord,
		human.x_coord,human.y_coord+wall_len);

} // END ERASE_HUMAN








/* ######################################################################
 MAKE_HUMAN
 ###################################################################### */

void make_human()
{
	human.x_coord=this_ship.init_x;
	human.y_coord=landing_line_1_y-wall_len;

} // END MAKE_HUMAN







/* ######################################################################
 MAKE_SHIP1
 ###################################################################### */
    
void make_ship1(double init_x,double init_y,int largest,int smallest)
{
        ship1.init_x=init_x;
        ship1.init_y=init_y;
        
        /* bottom left point */
        ship1.x_coord[0]= init_x-wall_len;
        ship1.y_coord[0]= init_y+wall_len;
        
        /* bottom right point */
        ship1.x_coord[1]= init_x+wall_len;
        ship1.y_coord[1]= init_y+wall_len;
        
        /* top right point */
        ship1.x_coord[2]= init_x+(wall_len/2);
        ship1.y_coord[2]= init_y-wall_len;
        
        /* top left point */
        ship1.x_coord[3]= init_x-(wall_len/2);
        ship1.y_coord[3]= init_y-wall_len;
        
        /* fire point */
        ship1.fire_point_x=init_x;
        ship1.fire_point_y=init_y+fire_len;
        
        /* initialize other variables */
        ship1.vertical_velocity=0;
        ship1.horizontal_velocity=0;
        
        ship1.angle=90;
        
        ship1.flame=0;
        
        ship1.points=ship_coords1;
        
} /* END MAKE_SHIP1 */


    

    

/* ######################################################################
 MAKE_SHIP2
 ###################################################################### */
    
void make_ship2(double init_x,double init_y,int largest,int smallest)
{
        ship2.init_x=init_x;
        ship2.init_y=init_y;
        
        /* bottom left point */
        ship2.x_coord[0]= init_x-wall_len;
        ship2.y_coord[0]= init_y+wall_len;
        
        /* bottom right point */
        ship2.x_coord[1]= init_x+wall_len;
        ship2.y_coord[1]= init_y+wall_len;
        
        /* bottom right leg */
        ship2.x_coord[2]= init_x+wall_len+leg_len;
        ship2.y_coord[2]= init_y+wall_len+leg_len;
        
        /* bottom right point */
        ship2.x_coord[3]= init_x+wall_len;
        ship2.y_coord[3]= init_y+wall_len;
        
        /* mid right point */
        ship2.x_coord[4]= init_x+wall_len+leg_len;
        ship2.y_coord[4]= init_y;
        
        /* top right point */
        ship2.x_coord[5]= init_x+(wall_len/2);
        ship2.y_coord[5]= init_y-wall_len;
        
        /* top left point */
        ship2.x_coord[6]= init_x-(wall_len/2);
        ship2.y_coord[6]= init_y-wall_len;
        
        /* mid left point */
        ship2.x_coord[7]= init_x-wall_len-leg_len;
        ship2.y_coord[7]= init_y;
        
        /* bottom left point */
        ship2.x_coord[8]= init_x-wall_len;
        ship2.y_coord[8]= init_y+wall_len;
        
        /* bottom left leg */
        ship2.x_coord[9]= init_x-wall_len-leg_len;
        ship2.y_coord[9]= init_y+wall_len+leg_len;
        
        /* fire point */
        ship2.fire_point_x=init_x;
        ship2.fire_point_y=init_y+fire_len;
        
        /* initialize other variables */
        ship2.vertical_velocity=0;
        ship2.horizontal_velocity=0;
        
        ship2.angle=90;
        
        ship2.flame=0;
        
        ship2.points=ship_coords2;
        
} /* END MAKE_SHIP2 */

    

    

    


/* ######################################################################
 MAKE_SHIP3
 ###################################################################### */
    
void make_ship3(double init_x,double init_y,int largest,int smallest)
{
        ship3.init_x=init_x;
        ship3.init_y=init_y;
        
        /* medium bottom left point */
        ship3.x_coord[0]= init_x-wall_len;
        ship3.y_coord[0]= init_y+wall_len;
        
        /*  medium bottom right point */
        ship3.x_coord[1]= init_x+wall_len;
        ship3.y_coord[1]= init_y+wall_len;
        
        /*  mid bottom right point */
        ship3.x_coord[2]= init_x+wall_len;
        ship3.y_coord[2]= init_y+(2*wall_len/3);
        
        /*  bottom right point */
        ship3.x_coord[3]= init_x+(3*wall_len/2);
        ship3.y_coord[3]= init_y+(2*wall_len/3);
        
        /* mid top point */
        ship3.x_coord[4]= init_x;
        ship3.y_coord[4]= init_y-wall_len;
       
        /*  bottom left point */
        ship3.x_coord[5]= init_x-(3*wall_len/2);
        ship3.y_coord[5]= init_y+(2*wall_len/3);

        /*  mid bottom left point */
        ship3.x_coord[6]= init_x-wall_len;
        ship3.y_coord[6]= init_y+(2*wall_len/3);
        
        /* fire point */
        ship3.fire_point_x=init_x;
        ship3.fire_point_y=init_y+fire_len;

        /* initialize other variables */
        ship3.vertical_velocity=0;
        ship3.horizontal_velocity=0;
        
        ship3.angle=90;
        
        ship3.flame=0;
        
        ship3.points=ship_coords3;

} /* END MAKE_SHIP3 */

    

    

    

    

/* ######################################################################
 MAKE_ARROW
 ###################################################################### */

void make_arrow(int x,int y)
{
        arrow.init_x=x;
        arrow.init_y=y;

} //END MAKE_ARROW

    

    

    

    

/* ######################################################################
 DRAW_ARROW
 ###################################################################### */

void draw_arrow()
{

    fprintf(fppo,"drawSegment %d %d %d %d\n",
            arrow.init_x,arrow.init_y+arrow_height,
            arrow.init_x,arrow.init_y);

    fprintf(fppo,"drawSegment %d %d %d %d\n",
            arrow.init_x,arrow.init_y,
            arrow.init_x+(arrow_height/2),arrow.init_y+(arrow_height/2));

    fprintf(fppo,"drawSegment %d %d %d %d\n",
            arrow.init_x,arrow.init_y,
            arrow.init_x-(arrow_height/2),arrow.init_y+(arrow_height/2));

} //END DRAW_ARROW

    

    
    

    

/* ######################################################################
 ERASE_ARROW
 ###################################################################### */

void erase_arrow()
{

    fprintf(fppo,"eraseSegment %d %d %d %d\n",
            arrow.init_x,arrow.init_y+arrow_height,
            arrow.init_x,arrow.init_y);

    fprintf(fppo,"eraseSegment %d %d %d %d\n",
            arrow.init_x,arrow.init_y,
            arrow.init_x+(arrow_height/2),arrow.init_y+(arrow_height/2));

    fprintf(fppo,"eraseSegment %d %d %d %d\n",
            arrow.init_x,arrow.init_y,
            arrow.init_x-(arrow_height/2),arrow.init_y+(arrow_height/2));

} //END ERASE_ARROW
        

        

        



/* ######################################################################
 HANDLE_ARROW
 ###################################################################### */

void handle_arrow(int signal)
{
    if( signal != SIGALRM )
    {
        return;
    }

    draw_arrow();
    fflush(fppo);
    erase_arrow();

}// END HANDLE_ARROW








/* ######################################################################
 DRAW_SHIPS
 ###################################################################### */

void draw_ships(struct a_ship ship)
{
    int i;

/*---------------------------------------------------------------------------*/
    
    for(i=0;i<ship.points;i++)
    {
        if(i<ship.points-1)
        {
            fprintf(fppo,"drawSegment %.0lf %.0lf %.0lf %.0lf\n",
                    ship.x_coord[i],ship.y_coord[i],
                    ship.x_coord[i+1],ship.y_coord[i+1]);
        }
        
        else
        {
            fprintf(fppo,"drawSegment %.0lf %.0lf %.0lf %.0lf\n",
                    ship.x_coord[i],ship.y_coord[i],
                    ship.x_coord[0],ship.y_coord[0]);
        }
    }

} /* END DRAW_SHIPS */









/* ######################################################################
 ERASE_SHIPS
 ###################################################################### */

void erase_ships(struct a_ship ship)
{
    int i;

/*---------------------------------------------------------------------------*/

    for(i=0;i<ship.points;i++)
    {
        if(i<ship.points-1)
        {
            fprintf(fppo,"eraseSegment %.0lf %.0lf %.0lf %.0lf\n",
                    ship.x_coord[i],ship.y_coord[i],
                    ship.x_coord[i+1],ship.y_coord[i+1]);
        }

        else
        {
            fprintf(fppo,"eraseSegment %.0lf %.0lf %.0lf %.0lf\n",
                    ship.x_coord[i],ship.y_coord[i],
                    ship.x_coord[0],ship.y_coord[0]);
        }
    }

} /* END ERASE_SHIPS */







/* ######################################################################
 PLACE_FLAG
 ###################################################################### */

void place_flag(int x,int y)
{
	fprintf(fppo,"drawSegment %.0lf %.0lf %.0lf %.0lf\n",
		human.x_coord+flag_dist,human.y_coord+wall_len,
        	human.x_coord+flag_dist,human.y_coord);

	fprintf(fppo,"drawSegment %.0lf %.0lf %.0lf %.0lf\n",
		human.x_coord+flag_dist,human.y_coord,
        	human.x_coord+flag_dist*2,human.y_coord+(wall_len/3));

	fprintf(fppo,"drawSegment %.0lf %.0lf %.0lf %.0lf\n",
		human.x_coord+flag_dist*2,human.y_coord+(wall_len/3),
        	human.x_coord+flag_dist,human.y_coord+(2*wall_len/3));

} //END PLACE_FLAG






