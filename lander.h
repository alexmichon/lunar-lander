/*
name:               Alex Michon
ONE Card number:    1392360
Unix id:            amichon
lecture section:    A1
lab section:        D01
TA's name:          Chenyang Huang */

#ifndef LANDER_H
#define LANDER_H

/*####################################################*/

/* The maximum length for an input line.*/
#define line_len 257

/* Landscape line return value */
#define landscape_rval 2

/* Max points of landscape */
#define max_points 21

/* Ship coordinates */
#define ship_start_x 330
#define ship_start_y 20

/* Cursor coordinates */
#define cursor_start_x 0
#define cursor_start_y 350

/* number of coords of ship */
#define ship_coords1 4
#define ship_coords2 10
#define ship_coords3 7

/* size of ship walls */
#define wall_len 10

/* size of thrust flame */
#define fire_len 15

/* Degrees of rotation */
#define degrees 10

/* 180 degrees */
#define oneeighty 180.0

/* change in time per frame */
#define time_change 0.0005

/* points in a piece */
#define piece_points 3

// number of pieces
#define number_of_pieces 4

// softens thrust jump
#define soft 20

// cursor length
#define cursor_len 7

// cursor movement speed
#define cursor_speed 25

// human movement speed
#define human_speed 3

// sketchpad size
#define sketch_left 0
#define sketch_right 637
#define sketch_very_top 0
#define sketch_top 100
#define sketch_bottom 455
#define sketch_very_bottom 460

// slow enough landing
#define landing_speed 10

// start ships
#define ship_start_x_1 200
#define ship_start_x_2 300
#define ship_start_x_3 400

// arrow y coord
#define arrow_y 40

// arrow lenghts
#define arrow_height 10
#define arrow_width 3
#define leg_len 5

// sketchpad ceiling
#define fix_roof 1

// flag distance
#define flag_dist 2

/*####################################################*/
struct a_ship
{
	double init_x;
	double init_y;

	double x_coord[ship_coords2];
	double y_coord[ship_coords2];

	double fire_point_x;
	double fire_point_y;

	double horizontal_velocity;
	double vertical_velocity;

	int angle;

	int flame;

	int points;
};
/*####################################################*/
struct landscape
{
	double x_coord[max_points];
	double y_coord[max_points];

	int points;
};
/*####################################################*/
struct a_human
{
	double x_coord;
	double y_coord;

};
/*####################################################*/
struct a_cursor
{
	double x;
	double y;
};
/*####################################################*/
struct an_arrow
{
    int init_x;
    int init_y;

};
/*####################################################*/

FILE *fppo;

int largest;
int smallest;
int difference;
int use_addition=0;
int crashed=0;
int landed=0;

// landing line
int landing_line_1_y;
int landing_line_2_y;
int landing_line_1_x;
int landing_line_2_x;

//fuel globals
int lower_f;
int lower;
int upper_f=22;
int left_f;
int right_f;
int out_of_fuel=0;
int amount_of_fuel;

// landscape variables
int no_landscape=0;

// exploring variables
int exploring = 0;

// choosing variables
int chooses=0;

double time1=0;
double time2;
double gravity;
double thrust;
double xA;
double yA;

struct a_ship this_ship;
struct landscape this_landscape;
struct a_cursor cursor;
struct a_human human;

struct a_ship ship1;
struct a_ship ship2;
struct a_ship ship3;

struct an_arrow arrow;

/*####################################################*/

/* Function declarations for draw3.c functions. */

// Main functions
int main(int argc, char *argv[]);
void handle_signal(int signal);

// landing functions
int is_flat();
int crash_check();

// ship functions
void make_ship(double init_x,double init_y,int largest,int smallest);
void draw_ship();
void erase_ship();
void ship_thrust();
void rotate_ship(int deg);
void ship_velocity();

// landscape functions
void make_landscape(FILE *landscape_image);
void draw_landscape();
void max_min_x();

// cursor functions
void draw_cursor();
void erase_cursor();
void handle_cursor(int signal);

// ncurses functions
void init_ncurses();
void shutdown_ncurses();

// loop around funtions
void loop_right();
void loop_left();

// fuel functions
void make_fuel();
void use_fuel();
void erase_fuel();

// explore functions
void handle_explore(int signal);
void draw_human();
void erase_human();
void make_human();
void place_flag();

// arrow functions
void draw_arrow();
void erase_arrow();
void make_arrow(int x,int y);
void handle_arrow(int signal);

// ship choosing functions
void make_ship1(double init_x,double init_y,int largest,int smallest);
void make_ship2(double init_x,double init_y,int largest,int smallest);
void make_ship3(double init_x,double init_y,int largest,int smallest);
void draw_ships(struct a_ship ship);
void erase_ships(struct a_ship ship);

/*####################################################*/

#endif
