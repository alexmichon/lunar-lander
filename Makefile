#name:               Alex Michon
# Creates a draw executable, and provides a make clean and run shortcut

lander: lander.o
	gcc -Wall -std=c99 lander.c -g -o lander -lm -lncurses

lander.o: lander.c
	gcc -Wall -std=c99 -c lander.c

run: 
	./lander -g 0 -t -20 -f landscape.txt

gravity:
	./lander -g 2 -t -20 -f landscape.txt

fuel: 
	./lander -g 0 -t -20 -f landscape.txt -i 10

land:
	./lander -g 0 -t -20 -f custom

choose:
	./lander -g 0 -t -20 -f landscape.txt -c

everything:
	./lander -g 0 -t -20 -f custom -i 10 -c

fun:
	./lander -g 0 -t -20 -f landscape.txt -i 100 -c

clean:
	-rm -f *.o lander core
	-rm -f *.o stdout core
	-rm -f *.o sketchpad.out core
